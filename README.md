# README #

### What is this repository for? ###

* Patch for FreeBSD 10.1  to enable Wake on Lan on bge interfaces 
* Adapted from the nas4free patch for 9.1
 

### How do I get set up? ###

* cd  /usr/src/sys/dev/bge/
* patch if_bge.c < if_bge.c.patch


### Thanks to zoon01 and daoyama ###

* free4nas 9.1. Patch on Sourceforge [http://sourceforge.net/p/nas4free/code/HEAD/tree/branches/9.1.0.1/build/kernel-patches/bge/files/patch-if_bge.diff#l25](http://sourceforge.net/p/nas4free/code/HEAD/tree/branches/9.1.0.1/build/kernel-patches/bge/files/patch-if_bge.diff#l25)